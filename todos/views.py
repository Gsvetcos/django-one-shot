from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_name = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": list_name,

    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()

            return redirect("todo_list_list")
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, pk):
    todo_list = get_object_or_404(TodoList, id=pk)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_list_object": todo_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, pk):
    todo_list = TodoList.objects.get(id=pk)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()

            return redirect("todo_list_list")
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, pk):
    todo_item = get_object_or_404(TodoItem, id=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.pk)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "todo_list_object": todo_item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
